package ru.pilot.skillbox.socnet.users.entity;

public enum Gender {
    MALE,
    FEMALE
}
