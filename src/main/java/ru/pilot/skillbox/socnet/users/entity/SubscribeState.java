package ru.pilot.skillbox.socnet.users.entity;

public enum SubscribeState {
    ACTIVE,
    BLOCK
}
