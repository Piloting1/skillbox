package ru.pilot.skillbox.socnet.users;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("unitTest")
class UsersApplicationTests {

	@Test
	void contextLoads() {
	}

}
